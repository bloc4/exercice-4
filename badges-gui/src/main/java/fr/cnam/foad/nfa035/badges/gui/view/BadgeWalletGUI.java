package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.context.ApplicationContextProvider;
import fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController;
import fr.cnam.foad.nfa035.badges.gui.controller.renderer.BadgeSizeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.controller.renderer.DefaultBadgeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Commentez-moi
 */
@Component("badgeWallet")
@Order(value = 2)
public class BadgeWalletGUI {

    private JButton button1;
    private JPanel panelParent;
    private JTable table1;
    private JPanel panelImageContainer;
    private JScrollPane scrollBas;
    private JScrollPane scrollHaut;
    private JPanel panelHaut;
    private JPanel panelBas;
    private BadgePanel badgePanel;


    private BadgesModel tableModel;
    private List<DigitalBadge> tableList;

    @Autowired
    private AddBadgeDialog addBadgeDialog;

    @Autowired
    private BadgeWalletController badgesWalletController;

    /**
     * Constructeur
     * Commentez-moi et décrivez le méchanisme de Forms
     */
    public BadgeWalletGUI() {
        // Déplacé dans postConstruct pour pouvoir prendre la main avec Spring
        // Et permettre d'accéder au contexte d'injection
    }


    @PostConstruct
    public void postConstruct(){
        button1.addActionListener(e -> {
            addBadgeDialog.pack();
            addBadgeDialog.setLocationRelativeTo(null);
            addBadgeDialog.setIconImage(addBadgeDialog.getIcon().getImage());
            addBadgeDialog.setVisible(true);
        });

        //TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
        tableModel = new BadgesModel(tableList);
        badgePanel.setPreferredSize(scrollHaut.getPreferredSize());
        table1.setModel(tableModel);
        table1.setRowSelectionInterval(0, 0);

        // 2. Les Renderers...
        table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
        table1.setDefaultRenderer(Object.class, new DefaultBadgeCellRenderer());
        // Apparemment, les Objets Number sont traités à part, donc il faut le déclarer explicitement en plus de Object
        table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
        // Idem pour les Dates
        table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());

        // 3. Tri initial
        table1.getRowSorter().toggleSortOrder(0);

        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                super.mouseClicked(event);
                if (event.getClickCount() == 2) {
                    int row = ((JTable) event.getComponent()).getSelectedRow();
                    badgesWalletController.loadBadge(row, getInstance());
                }
            }
        });

    }

    /**
     * Commentez-moi
     *
     * @param badge le badge
     */
    public void delegateSetAddedBadge(DigitalBadge badge) {
        badgesWalletController.setAddedBadge(badge, this);
    }

    /**
     * Commentez-moi
     *
     * @return ImageIcon
     */
    public ImageIcon getIcon() {
        return new ImageIcon(Objects.requireNonNull(getClass().getResource("/logo.png")));
    }

    /**
     * Commentez-moi
     */
    private void createUIComponents() {
        try {
            this.badgesWalletController = ApplicationContextProvider.getApplicationContext().getBean("badgesWalletController", BadgeWalletController.class);

            badgesWalletController.delegateUIComponentsCreation(this);
            badgesWalletController.delegateUIManagedFieldsCreation(this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * Commentez-moi
     *
     * @param tableList le model
     */
    public void setTableList(List<DigitalBadge> tableList) {
        this.tableList = tableList;
    }

    /**
     * Commentez-moi
     *
     * @return JPanel
     */
    public JPanel getPanelImageContainer() {
        return panelImageContainer;
    }

    /**
     * Commentez-moi
     *
     * @param panelImageContainer le conteneur d'image
     */
    public void setPanelImageContainer(JPanel panelImageContainer) {
        this.panelImageContainer = panelImageContainer;
    }

    /**
     * Commentez-moi
     *
     * @param badgePanel le composant d'affichage du badge (image)
     */
    public void setBadgePanel(BadgePanel badgePanel) {
        this.badgePanel = badgePanel;
    }

    /**
     * Commentez-moi
     *
     * @return BadgesModel
     */
    public BadgesModel getTableModel() {
        return tableModel;
    }

    /**
     * Commentez-moi
     *
     * @return JTable
     */
    public JTable getTable1() {
        return table1;
    }

    /**
     * Commentez-moi
     *
     * @return JScrollPane
     */
    public JScrollPane getScrollHaut() {
        return scrollHaut;
    }

    /**
     * Commentez-moi
     *
     * @return JPanel
     */
    public JPanel getPanelHaut() {
        return panelHaut;
    }

    /**
     * Commentez-moi
     *
     * @return List<DigitalBadge>
     */
    public List<DigitalBadge> getTableList() {
        return tableList;
    }

    /**
     * Facilité pour accéder à cette instance depuis un déclaration anonyme
     *
     * @return BadgeWalletGUI
     */
    private BadgeWalletGUI getInstance() {
        return this;
    }

    /**
     * Commentez-moi
     *
     * @return le panneau parent
     */
    public JPanel getPanelParent() {
        return panelParent;
    }

}
