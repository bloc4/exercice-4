package fr.cnam.foad.nfa035.badges.service;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.http.ResponseEntity;

import java.util.Set;

public interface BadgesWalletRestService {

    void deleteBadge();

    ResponseEntity<Set<DigitalBadge>> getMetadata();
    void putBadge();

    void readBadge();




}
