package fr.cnam.foad.nfa035.badges.service.impl;

import fr.cnam.foad.nfa035.badges.service.BadgesWalletRestService;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Set;

@RestController


public class BadgesWalletRestServiceImpl implements BadgesWalletRestService {

    @Qualifier("directAccess")
    @Autowired
    DirectAccessBadgeWalletDAO dao;

    /**
     * Lecture du Wallet => R
     *
     * @return ResponseEntity la réponse REST toujours
     */

    @Operation(summary = "Récupère le métadonnées du Wallet",
            description = "Récupère le métadonnées du portefeuille, c'est à dire l'index des badges qui s'y trouve"
    )
    @Tag(name = "getMetadata")
    @GetMapping("/metas")
    ResponseEntity<Set<DigitalBadge>> getMetadata(JSONBadgeWalletDAOImpl jsonBadgeDao) throws IOException {


        return ResponseEntity
                .ok()
                .body(jsonBadgeDao.getWalletMetadata());


    }

    @Override
    public void deleteBadge() {

    }

    @Override
    public ResponseEntity<Set<DigitalBadge>> getMetadata() {
        return null;
    }

    @Override
    public void putBadge() {

    }

    @Override
    public void readBadge() {

    }
}