package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class MetadataDeserializerDatabaseImpl implements MetadataDeserializer {

    private static final Logger LOG = LogManager.getLogger(MetadataDeserializerDatabaseImpl.class);

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public Set<DigitalBadge> deserialize(WalletFrameMedia media) throws IOException {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        BufferedReader br = media.getEncodedImageReader(false);
        return br.lines()
                .map(
                    l-> l.split(";")
                )
                .filter(s -> s.length == 7)
                .map(
                        s -> {
                            Date begin=null,end=null;
                            try {
                                begin = format.parse(s[4]);
                            } catch (ParseException e) {
                                LOG.error("Problème de parsage, on considère la date Nulle", e);
                            }
                            try {
                                end = format.parse(s[5]);
                            } catch (ParseException e) {
                                LOG.error("Problème de parsage, on considère la date Nulle", e);
                            }
                            return new DigitalBadge(s[3],begin,end,new DigitalBadgeMetadata(Integer.parseInt(s[0]),Long.parseLong(s[1]),Long.parseLong(s[2])),null);
                        }
                )
                .collect(Collectors.toSet());
    }

    /**
     * Peremet de lire la dernière ligne d'un fichier en accès direct
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length-1; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n')
            {
                builder.append(c);
            }
            else{
                break;
            }
        }
        return builder.reverse().toString();
    }

}
